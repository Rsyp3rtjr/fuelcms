<?php
class Resume extends CI_Controller {
    public function _construct()
    {
        parent::_construct();
      
    }
    
    public function index()
    {
        // load the fuel_page library class and pass
        // it the view file you want to load
        
        //echo "gets here";
    }
    
    function htmlResume(){
        $vars['whichPage'] = 'resumes';
        $this->fuel->pages->render('resumes/resume',$vars);
    }
    
    function pdfResume(){
        $vars['pdfFilePath'] = pdf_path('RichardSypertJrResumePDF.pdf');
        $vars['pdfFileName'] = 'Resume for Richard L. Sypert';
        $this->fuel->pages->render('showPDF/showAPDFDoc',$vars);
    }
    
 
}