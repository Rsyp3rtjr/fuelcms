<?php
class Techwriting extends CI_Controller {
    public function _construct()
    {
        parent::_construct();
      
    }
    
    public function index()
    {
        // load the fuel_page library class and pass
        // it the view file you want to load
        
        echo "gets here";
    }
    
    function writing(){
        $vars['whichPage'] = 'techWriter';
        $this->fuel->pages->render('techWriting/techWriting',$vars);
    }
    
    function engSpec(){
        $vars['pdfFilePath'] = pdf_path('engSpec.pdf');
        $vars['pdfFileName'] = 'Engineering Specification';
        $this->fuel->pages->render('showPDF/showAPDFDoc',$vars);
    }
    
    function graingerABCDE(){
        $vars['pdfFilePath'] = pdf_path('graingerABCDE.pdf');
        $vars['pdfFileName'] = 'Grainger ABCDE Maintenance Manual';
        $this->fuel->pages->render('showPDF/showAPDFDoc',$vars);
    }
    
     function graingerCDE(){
        $vars['pdfFilePath'] = pdf_path('graingerCDE.pdf');
        $vars['pdfFileName'] = 'Grainger CDE Maintenance Manual';
        $this->fuel->pages->render('showPDF/showAPDFDoc',$vars);
    }
    
    function mecPManual(){
        $vars['pdfFilePath'] = pdf_path('mecProductManual.pdf');
        $vars['pdfFileName'] = 'MEC Product Maintenance Manual';
        $this->fuel->pages->render('showPDF/showAPDFDoc',$vars);
    }
    
     function whitePaper(){
        $vars['pdfFilePath'] = pdf_path('whitePaper.pdf');
        $vars['pdfFileName'] = 'Technical White Paper';
        $this->fuel->pages->render('showPDF/showAPDFDoc',$vars);
    }
    
    function softwareTW(){
        $vars['whichPage'] = 'codeDev';
        $this->fuel->pages->render('techWriting/softwareTW',$vars);
    }
    
}