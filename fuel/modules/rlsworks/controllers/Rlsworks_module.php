<?php
require_once(FUEL_PATH.'/libraries/Fuel_base_controller.php');

class Rlsworks_module extends Fuel_base_controller {
	
	public $nav_selected = 'rlsworks|rlsworks/:any';

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$vars['page_title'] = $this->fuel->admin->page_title(array(lang('module_rlsworks')), FALSE);
		$crumbs = array('tools' => lang('section_tools'), lang('module_rlsworks'));

		$this->fuel->admin->set_titlebar($crumbs, 'ico_rlsworks');
		$this->fuel->admin->render('_admin/rlsworks', $vars, '', RLSWORKS_FOLDER);
	}
}